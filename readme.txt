A template project for a smooth start with Java and JPA over Hibernate & Postgres based on Maven.

To get started:

* Install latest versions of jdk 8, docker, and maven on your system.
* Start the database by opening a console and executing the script startDb.sh (For linux, otherwise adjust to fit your environment)
* Start the Java program by opening another console, executing the script start.sh

Once the database process has been stopped, no data will be retained.

Adjust and redistribute project at will.

In order to extend the project, import it through your IDE of choice, e.g. Eclipse, as a "Maven Project".

In order to investigate the database, install psql on you system and then execute:

"psql -h 172.17.0.2 -p 5432 -U jee_user jee"

In order to change the executables main class, substitute the mainClass in pom.xml.

You might have to substitute the IP address (how to get it can be seen in start.sh).

Felix Dobslaw
