#! /bin/bash

set -e

# Getting hold of the database IP
IP=$(docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' jee_db)

# Building the executable jar
mvn clean install

# Executing the program
java -jar target/jpa-template.jar $IP

echo
echo "-------------------------"
echo "The database IP is ${IP}"
echo "-------------------------"
